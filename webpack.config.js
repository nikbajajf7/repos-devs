module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'src/utils/icons/[name].[ext]',
            },
          },
        ],
      },
    ],
  },
};