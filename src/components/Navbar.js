import React from "react"

const Navbar = ({children}) => (
    <div>
        <div className="header">
            <h3 className="text-center trending">Trending</h3>
            <p className="text-center header-para">See what the GitHub community is most excited about today.</p>
        </div>
        {children}
    </div>
)

export default Navbar