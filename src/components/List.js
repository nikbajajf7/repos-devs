import React from "react"
import { useHistory, useLocation } from "react-router-dom";

const List = ({ children }) => {
    const { pathname = "/" } = useLocation();
    const history = useHistory()

    const changeRoute = (route) => {
        if(pathname !== route){
            history.push(route)
        }
    }

    return <div  className="list">
        <div className="list-header">
            <button
                className={`buttons button-repo ${pathname === "/" ? "active-button" : ""}`}
                onClick={() => changeRoute("/")}
            >
                Repositories
            </button>
            <button
                className={`buttons button-dev ${pathname === "/developers" ? "active-button" : ""}`}
                onClick={() => changeRoute("/developers")}
            >
                Developers
            </button>
        </div>
        <div>
            {children}
        </div>
    </div>
}

export default List