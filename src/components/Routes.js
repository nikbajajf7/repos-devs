import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import List from "./List"
import Repos from "../containers/Repos"
import Developers from "../containers/Developers"

const Routes = () => (
    <Router>
        <List>
          <Switch>
            <Route path="/" exact={true}>
              <Repos />
            </Route>
            <Route path="/developers" exact={true}>
              <Developers />
            </Route>
          </Switch>
        </List>
    </Router>
)

export default Routes