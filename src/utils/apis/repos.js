import axios from "axios"

export const fetchRepos = async () => {
    return await axios.get("https://cors-anywhere.herokuapp.com/https://gh-trending-api.herokuapp.com/repositories")
}