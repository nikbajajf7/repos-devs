import React from "react"
import { useQuery } from "react-query";

import { fetchDevelopers } from "../utils/apis/developers"
import developers from "../utils/mocks/developers"
import  { ReactComponent as RepoFolderLogo } from '../utils/icons/folder.svg';
import  { ReactComponent as FireLogo } from '../utils/icons/fire.svg';
import  { ReactComponent as HeartLogo } from '../utils/icons/heart.svg';

const Developers = () => {
    const { data = { data: developers } } = useQuery('developers', fetchDevelopers) // Passing mock data if request gets cors error.

    return (
        <div>
            {data.data.map((dev) => (
                <div className="developers" key={dev.rank}>
                    <div className="developers-left-section">
                        <p className="developer-seq">{dev.rank}</p>
                        <img src={dev.avatar} alt="developer" className="developer-image"/>
                        <div className="developer-name-info">
                            <a href={dev.url} target="_blank" rel="noreferrer" className="repo-name-link">
                                <h3 className="developer-name">{dev.name}</h3>
                                <p className="deveoper-username">{dev.username}</p>
                            </a>
                        </div>
                    </div>
                    <div className="developers-right-section">
                        <div className="dev-repo-info">
                            <div className="dev-popular-repo">
                                <FireLogo className="fire-icon"/>
                                <p className="popular-repo-para">
                                    Popular repo
                                </p>
                            </div>
                            <div className="dev-repo-name">
                                <RepoFolderLogo className="icons"/>
                                <a href={dev.popularRepository.url} target="_blank" rel="noreferrer" className="repo-name-link">
                                    <h3 className="dev-repo-name-h3">{dev?.popularRepository?.repositoryName}</h3>
                                </a>
                            </div>
                            <p className="dev-repo-description">{dev.popularRepository?.description}</p>
                        </div>
                        <div className="dev-repo-actions">
                            <button className="buttons dev-sponsor-button">
                                <HeartLogo className="heart-icon"/>
                                Sponsor
                            </button>
                            <button className="buttons dev-follow-button">Follow</button>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Developers