import React from "react"
import { useQuery } from 'react-query'

import { fetchRepos } from "../utils/apis/repos"
import repos from "../utils/mocks/repos"
import  { ReactComponent as StarLogo } from '../utils/icons/star.svg';
import  { ReactComponent as ForkLogo } from '../utils/icons/fork.svg';
import  { ReactComponent as RepoFolderLogo } from '../utils/icons/folder.svg';

const Repos = () => {
    const { data = { data: repos } } = useQuery('repos', fetchRepos) // Passing mock data if request gets cors error.

    return<div>
        {data.data.map((repo) => (
            <div className="repos" key={repo.rank}>
                <div className="repo-header">
                <a href={repo.url} target="_blank" className="repo-name-link" rel="noreferrer">
                    <h3 className="repo-name">
                        <RepoFolderLogo className="icons"/>
                        {`${repo.username} / ${repo.repositoryName}`}
                    </h3>
                </a>
                <button className="repo-star-button">
                    <StarLogo className="icons"/>
                    Star
                </button>
                </div>
                <p className="description">{repo.description}</p>
                <div className="repo-info">
                    <div className="repo-info-left">
                        <p className="repo-language">
                            {repo.language}
                        </p>
                        <p className="repo-stars">
                            <StarLogo className="icons"/>
                            {repo.totalStars}
                        </p>
                        <p className="repo-forks">
                            <ForkLogo className="icons"/>
                            {repo.forks}
                        </p>
                        <div className="repo-info-right">
                            <p>Build by</p>
                            {repo.builtBy.map((dev) => (
                                <img src={dev.avatar} alt="Dev" className="dev-avatar" key={dev.avatar}/>
                            ))}
                        </div>
                    </div>
                    <p className="repo-stars-today">
                        <StarLogo className="icons"/>
                        {repo.starsSince} stars today
                    </p>
                </div>
            </div>
        ))}
    </div>
}

export default Repos