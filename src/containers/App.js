import React from "react";
import { QueryClient, QueryClientProvider } from 'react-query'

import Navbar from "../components/Navbar"
import Routes from "../components/Routes"

const queryClient = new QueryClient()

const App = () => {
  return (
        <QueryClientProvider client={queryClient}>
            <Navbar>
                <Routes />
            </Navbar>
        </QueryClientProvider>
    );
}

export default App